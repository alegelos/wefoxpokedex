//
//  AppUtils.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct AppUtils {
    func getRandom(from: Int = 1, to: Int = 1000) -> Int {
        return Int.random(in: from ... to)
    }
}
