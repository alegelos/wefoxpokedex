//
//  UserDefaultUtils.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/28/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct UserDefaultUtils {
    func getValue<T: Decodable>(forKey key: String) -> Result<T, ErrorResult>{
        let result = getDataValue(forKey: key)
        switch result {
        case .failure(let error):
            return Result.failure(error)
        case .success(let data):
            guard let value = try? JSONDecoder().decode(T.self, from: data) else{
                return Result.failure(ErrorResult.parser(string: "fail to decode to \(T.self)"))
            }
            return Result.success(value)
        }
    }
    
    func set<T: Codable>(value: T, forKey key: String) -> Result<T, ErrorResult>{
        do {
            let pokemonData = try JSONEncoder().encode(value)
            UserDefaults.standard.set(pokemonData, forKey: key)
            return Result.success(value)
        } catch {
            return Result.failure(ErrorResult.parser(string: error.localizedDescription))
        }
    }
}

//MARK: - Private methods
extension UserDefaultUtils{
    private func getDataValue(forKey key: String) -> Result<Data, ErrorResult> {
        guard let value = UserDefaults.standard.object(forKey: key) as? Data else{
            return Result.failure(ErrorResult.custom(string: "value not stored"))
        }
        return Result.success(value)
    }
}
