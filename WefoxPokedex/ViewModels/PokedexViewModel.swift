//
//  PokedexViewModel.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/25/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

final class PokedexViewModel {
    let userDefaults: UserDefaultUtils

    var results = Box([Pokemon]())
    var didSelectItem: Box<Int?> = Box(nil)
    
    init(withUserDefaults userDefaults: UserDefaultUtils = UserDefaultUtils()) {
            self.userDefaults = userDefaults
    }
    
    func getHuntedPokemons() {
        let huntPokemons: Result<[Pokemon], ErrorResult> = userDefaults.getValue(forKey: UserDefaultKeys.pokemons.rawValue)
        switch huntPokemons {
        case .failure(let error):
            results.value.removeAll()
            print(error)
            //Add alert or something with error
        case .success(let pokemons):
            results.value = pokemons
        }
    }
    
    func updateHuntedPokemons() {
        userDefaults.set(value: results.value, forKey: UserDefaultKeys.pokemons.rawValue)
    }
}
