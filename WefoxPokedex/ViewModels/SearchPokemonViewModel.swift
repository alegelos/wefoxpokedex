//
//  SearchPokemonViewModel.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/26/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
import Moya

final class SearchPokemonViewModel{
    var state: Box<State<Pokemon, ErrorResult>> = Box(State.loading)
    let provider = MoyaProvider<PokeApi>()
    
    func searchPokemon(withID id: Int = AppUtils().getRandom()) {
        provider.request(PokeApi.searchPokemon(id)) {[weak self] result in
            switch result{
            case .success(let response):
                do {
                    self?.state.value = State.ready(try response.map(PokeApiResponse<Pokemon>.self).data.results)
                } catch {
                    self?.state.value = State.error(ErrorResult.parser(string: error.localizedDescription))
                }
            case .failure(let error):
                self?.state.value = State.error(ErrorResult.network(string: error.localizedDescription))
            }
        }
    }
}
