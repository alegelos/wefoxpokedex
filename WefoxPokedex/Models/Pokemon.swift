//
//  Pokemon.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

struct Pokemon: Codable {
    var name:     String?
    var imageURL: String?
    var capture:  String?
    var weight:   String?
    var height:   String?
    var exp:      String?
    var type:     String?
}
