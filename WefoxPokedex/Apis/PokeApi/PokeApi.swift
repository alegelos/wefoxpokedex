//
//  PokeApi.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/24/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation
import Moya

enum PokeApi {
    case searchPokemon(Int)
}

struct PokeApiResponse<T: Codable>: Codable {
    let data: PokeApiResult<T>
}

struct PokeApiResult<T: Codable>: Codable {
    let results: [T]
}

extension PokeApi: TargetType{
    var baseURL: URL {
        guard let baseURL = URL(string: "https://pokeapi.co/api/v2/") else{
            fatalError("PokeAPI: Fail to get base URL")
        }
        return baseURL
    }

    var path: String {
        switch self {
        case .searchPokemon(let pokemonId): return "pokemon/\(pokemonId)"
        }
    }

    var method: Moya.Method {
        switch self {
        case .searchPokemon: return .get
        }
    }

    var sampleData: Data {
        return Data()
    }

    var task: Task {
        switch self {
        case .searchPokemon: return .requestPlain
        }
    }

    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
    var validationType: ValidationType {
        return .successCodes
    }
}
