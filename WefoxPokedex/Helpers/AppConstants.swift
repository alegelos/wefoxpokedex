//
//  AppConstants.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/25/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

enum CellIdentifiers: String {
    case pokedexCell
}

enum UserDefaultKeys: String {
    case pokemons
}
