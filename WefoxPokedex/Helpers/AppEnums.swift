//
//  AppEnums.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/28/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

enum State<T: Codable, E: Error> {
    case loading, ready([T]), error(E)
}

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}

enum ErrorResult: Error {
    case network(string: String)
    case parser(string:  String)
    case custom(string:  String)
}
