//
//  Box.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/26/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import Foundation

import Foundation

final class Box<T> {
    typealias Listener = (T) -> Void
    var listener: Listener?
    
    var value: T {
        didSet {
            listener?(value)
        }
    }
    
    init(_ value: T) {
        self.value = value
    }
    
    func bind(listener: Listener?) {
        self.listener = listener
        listener?(value)
    }
}
