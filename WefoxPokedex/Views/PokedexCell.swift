//
//  PokedexCell.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/25/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit
import Kingfisher

final class PokedexCell: UICollectionViewCell {
    
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subTitle: UILabel!
    
    func loadCell(withPokemon pokemon:Pokemon) {
        title.text    = (pokemon.name ?? "") + " - " + (pokemon.type ?? "")
        subTitle.text = pokemon.exp
        
        loadCoverImage(fromURL: pokemon.imageURL)
    }
}

//MARK: - Private methods
extension PokedexCell{
    private func loadCoverImage(fromURL url:String?){
        guard let urlString = url, let url = URL(string: urlString) else{return}
        
        coverImage.kf.setImage(with: url)
    }
}
