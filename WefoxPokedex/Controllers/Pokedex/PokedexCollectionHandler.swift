//
//  PokedexCollectionHandler.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/25/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

protocol PokedexCollectionHandlerDelegate: class {
    func reloadPages()
}

final class PokedexCollectionHandler: NSObject{
    weak var delegate: PokedexCollectionHandlerDelegate?
    
    var pokemons = [Pokemon](){
        didSet{
            delegate?.reloadPages()
        }
    }
}

//MARK: - UICollectionViewDataSource
extension PokedexCollectionHandler: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pokemons.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifiers.pokedexCell.rawValue, for: indexPath) as? PokedexCell else {
            fatalError("fail to deque pokedex cell")
        }
        cell.loadCell(withPokemon: pokemons[indexPath.item])
        return cell
    }
}

//MARK: - UICollectionViewDelegateFlowLayout
extension PokedexCollectionHandler: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200, height: 150)
    }
}
