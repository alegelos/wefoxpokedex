//
//  PokedexView.swift
//  WefoxPokedex
//
//  Created by Alejandro Gelos on 1/25/19.
//  Copyright © 2019 Alejandro Gelos. All rights reserved.
//

import UIKit

final class PokedexView: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    let collectionViewHandler = PokedexCollectionHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpCollectionView()
        setUpBinding()
    }
}

//MARK: - PokedexCollectionHandlerDelegate
extension PokedexView: PokedexCollectionHandlerDelegate {
    func reloadPages() {
        DispatchQueue.main.async { [weak self] in
            self?.collectionView.reloadData()
        }
    }
}

//MARK: - Private methods
extension PokedexView{
    private func setUpCollectionView(){
        collectionView.delegate   = collectionViewHandler
        collectionView.dataSource = collectionViewHandler
        collectionViewHandler.delegate = self
    }
    
    private func setUpBinding(){
        
    }
}

